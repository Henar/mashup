<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function buscarRanking($tipoRanking){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://examples.opendatasoft.com/api/records/1.0/search/?dataset=world-heritage-unesco-list&q=&rows=1052&facet=category&facet=country_en&facet=country_fr&facet=continent_en&facet=continent_fr");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $response = curl_exec($ch);
        
        $pics = json_decode($response, true);
        $datosRanking=[];
        $i=0;

           if($tipoRanking=='categoria'){      
                
               foreach ($pics['facet_groups']['0']['facets'] as $value) {
                    $i++;
                    $datosRanking[]=[
                        ('total'.$i)=>$value['count'], 
                        ('categoria'.$i)=>$value['name']
                    ];
                    
                
              }
           }elseif($tipoRanking=='pais'){
                foreach ($pics['facet_groups']['1']['facets'] as $value) {
                    $i++;
                    $datosRanking[]=[
                        ('total'.$i)=>$value['count'], 
                        ('pais'.$i)=>$value['name']
                    ];
                }
           }else{
                foreach ($pics['facet_groups']['4']['facets'] as $value) {
                    $i++;
                    $datosRanking[]=[
                        ('total'.$i)=>$value['count'], 
                        ('continente'.$i)=>$value['name']
                    ];
                }
           }
    
        
        
        return $datosRanking;
        // return $i;
        
    }
    
    

    public function categoria(){
       
        $lista=$this->buscarRanking('categoria');
        return view('ranking.index', compact('lista'));
    }

    public function paises(){
       
        $lista=$this->buscarRanking('pais');
        return view('ranking.index', compact('lista'));
    }

    public function continentes(){
       
        $lista=$this->buscarRanking('continente');
        return view('ranking.index', compact('lista'));
    }

}
