<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapaController extends Controller
{

    public function buscarLugar($id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://examples.opendatasoft.com/api/records/1.0/search/?dataset=world-heritage-unesco-list&q=&rows=1052&facet=category&facet=country_en&facet=country_fr&facet=continent_en&facet=continent_fr");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $response = curl_exec($ch);
        
        $pics = json_decode($response, true);
        $datosLugar=[];
        for ($i=0; $i < count($pics['records']); $i++) {
           if($pics['records'][$i]['recordid']==$id){
               if(array_key_exists('justification_en', $pics['records'][$i]['fields'])){
                    $extra= $pics['records'][$i]['fields']['justification_en'];
                }else{
                    $extra='';
                }
            $datosLugar=[
                'pais'=>$pics['records'][$i]['fields']['country_en'], 
                'continente'=>$pics['records'][$i]['fields']['continent_en'], 
                'nombre'=>$pics['records'][$i]['fields']['name_en'],
                'categoria'=>$pics['records'][$i]['fields']['category'], 
                'descripcion'=>$pics['records'][$i]['fields']['short_description_en'], 
                'fechaEntrada'=>$pics['records'][$i]['fields']['date_inscribed'],
                'latitud'=>$pics['records'][$i]['fields']['latitude'],
                'longitud'=>$pics['records'][$i]['fields']['longitude'],
                

            ];
           }
    
        }
        $datosLugar['textoExtra']= $extra;
        return $datosLugar;
        
    }

    public function imgagenFlickr($latitud, $longitud, $ciudad){
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=46d7c83f534201381005d17eeae9f3b0&format=json&tags=". 
                $ciudad. "&per_page=1&page=1&lat=". $latitud. "&lon=". $longitud);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $response= curl_exec($ch);
        // $respuesta=json_decode($response);
        
        $datos=explode('{', $response);
        
        $valoresImagen=[];
        
        if(count($datos)>3){
            $separa=explode(',', $datos[3]);
            
            $numId=(explode(':', $separa[0]))[1];
            $id=substr($numId, 1, -1);

            $numSecret=(explode(':', $separa[2]))[1];
            $secret=substr($numSecret, 1, -1);
            
            $numServer=(explode(':', $separa[3]))[1];
            $server=substr($numServer, 1, -1);

            $valoresImagen=[
                'server'=>$server, 
                'id'=>$id, 
                'secret'=>$secret
            ];
        }

        return $valoresImagen;
    }

    public function index(){
       
        // return $this->datosHunesco();
        // $datos= $this->datosHunesco();
		// return view('mapas.index', compact('datos'));
        return view('mapas.index');
    }

    public function show($nombre){
        $patrimonio=$this->buscarLugar($nombre);
        $img=$this->imgagenFlickr($patrimonio['latitud'], $patrimonio['longitud'], $patrimonio['pais']);
        $informacion=array_merge($patrimonio, $img);
		// print_r($informacion);
		return view('mapas.show', ["info"=>$informacion]);
    }
}
