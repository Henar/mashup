@extends('layouts.master')

@section('titulo')
    World Heritage
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <h1>{{$info['nombre']}}</h1>
        <br>
        
            <div class="col-sm-3">
                @if(count($info)>9)
                        <img src="https://live.staticflickr.com/{{$info['server']}}/{{$info['id']}}_{{$info['secret']}}.jpg" alt="Imagen de {{$info['nombre']}}. '">
                
                @endif
            </div>

            <br>

            <div class="col-sm-9">
                
                <h4>{{$info['descripcion']}}</h4>

                @if ($info['textoExtra']!='')
                    <p>{{$info['textoExtra']}}</p>
                @endif

                <p>Belongs to {{$info['pais']}} which is located in {{$info['continente']}}.</p>
                <p> It made its entry on the list the {{$info['fechaEntrada']}}.</p><p> This World Heritage it`s found in the {{$info['categoria']}} 
                category. </p>The cordinates where ist`s located are latitude: {{$info['latitud']}} and longitude: {{$info['longitud']}}. <br/>


                <br/><br/>
                <a href="../" class="btn btn-secondary">Volver al mapa</a>

                <br><br>

            </div>
        
    </form>
    
@endsection