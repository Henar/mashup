<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-warning">
  <a class="navbar-brand ml-5" href="{{url('/')}}">World Heritage</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a href="{{route('mapas.index')}}" class="nav-link {{ Request::is('mapas*') ? ' active' : ''}}">Mapa</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/ranking/categoria')}}" class="nav-link {{ Request::is('/ranking/categoria')? ' active' : ''}}">Ranking by Categories</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/ranking/paises')}}" class="nav-link {{ Request::is('ranking/paises')? ' active' : ''}}">Ranking by Countries</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/ranking/continentes')}}" class="nav-link {{ Request::is('ranking/continentes')? ' active' : ''}}">Ranking by Continents</a>
      </li>
    </ul>
    
  </div>
</nav>





