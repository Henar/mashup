@extends('layouts.master')

@section('titulo')
    World HERITAGE
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <h1>Ránking</h1>
        <br>

            <div class="col-sm-9">
                {{-- <p>{{$lista}}</p> --}}
                {{-- <p>{{count($lista)}}</p> --}}
                {{-- <p>{{print_r($lista)}}</p>  --}}
                
                @if (count($lista)<=3)
                    
                    @for($i=0; $i<count($lista); $i++)
                        {{-- <p>{{print_r($lista)}}</p> --}}
                        <ul>
                            <li>Categoria: {{strtoupper($lista[$i]['categoria'.($i+1)])}}-> <b>{{$lista[$i]['total'.($i+1)]}} </b> patrimonios</li>
                        </ul>
                    
                    @endfor
                
                @elseif (count($lista)>=100)
                    @for($i=0; $i<count($lista); $i++)
                        <ul>
                            <li>País: <b>{{strtoupper($lista[$i]['pais'.($i+1)])}}</b>-> <b>{{$lista[$i]['total'.($i+1)]}} </b> patrimonios</li>
                        </ul>
                
                    @endfor
                    
                @elseif(count($lista)>=5 && count($lista)<10)
                    @for($i=0; $i<count($lista); $i++)
                        <ul>
                            <li>Continente: {{strtoupper($lista[$i]['continente'.($i+1)])}}-> <b>{{$lista[$i]['total'.($i+1)]}} </b> patrimonios</li>
                        </ul>
                
                    @endfor
                    
                @endif
            </div>
        
    </form>
    
@endsection