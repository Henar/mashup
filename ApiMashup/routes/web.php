<?php

use App\Http\Controllers\InicioController;
use App\Http\Controllers\MapaController;
use App\Http\Controllers\RankingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, "__invoke"])->name('inicio.inicio');

Route::get('/mapa', [MapaController::class, "index"])->name('mapas.index');
Route::get('/mapa/{lugar}', [MapaController::class, "show"])->name('mapas.show');

Route::get('/ranking/categoria', [RankingController::class, "categoria"])->name('ranking.categoria');
Route::get('/ranking/paises', [RankingController::class, "paises"])->name('ranking.paises');
Route::get('/ranking/continentes', [RankingController::class, "continentes"])->name('ranking.continentes');
